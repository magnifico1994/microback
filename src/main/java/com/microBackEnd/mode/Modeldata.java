package com.microBackEnd.mode;

import java.io.Serializable;
 
public class Modeldata implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3797606747159902108L;
	private int code;
	private String mensage;
	private boolean estatus;
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMensage() {
		return mensage;
	}
	public void setMensage(String mensage) {
		this.mensage = mensage;
	}
	public boolean isEstatus() {
		return estatus;
	}
	public void setEstatus(boolean estatus) {
		this.estatus = estatus;
	}
}
