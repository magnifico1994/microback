package com.microBackEnd.rest;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.microBackEnd.mode.Modeldata;
@RestController
@RequestMapping(path = "/rest/dataMB")
public class restData {

	@GetMapping(path= "/obtenerDatos", produces = MediaType.APPLICATION_JSON_VALUE )
	public @ResponseBody Modeldata getData(@RequestParam (name = "msg") String message) {
		Modeldata response = new  Modeldata();
		response.setCode(200);
		response.setEstatus(true);
		response.setMensage("Hola mis parceros  vamos a ganar esta materia"+ message);
		return response;
	}
}
